/**
 * Created by jasonc on 7/24/16.
 */

// doing it the manual way
export default function({ dispatch }) {
    return next => action => {
        if(!action.payload || !action.payload.then) {
            return next(action);
        }

        action.payload
            .then(function(response) {
                // creating action with the old type, but replacing promise with new data
                const newAction = { ...action, payload: response };
                dispatch(newAction); // send it through everything again
            });
    }
}