/**
 * Created by jasonchhagan on 7/24/16.
 */

import React from "react";
import UserList from "./UserList";

export default class App extends React.Component {
    render() {
        return (
            <div className="container">
                <h1 className="text-center">Users List from API</h1>
                <UserList/>
            </div>
        )
    };
}