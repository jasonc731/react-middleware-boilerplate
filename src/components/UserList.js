/**
 * Created by jasonchhagan on 7/24/16.
 */

import React from "react";
import { connect } from "react-redux";
import * as actions from "../actions";

class UserList extends React.Component {

    // fetch the users
    componentWillMount() {
        this.props.fetchUsers();
    }

    renderUser(user) {
        return (
            <div className="jumbotron">
                <div className="text-center">
                    <h4 className="card-title">{user.name}</h4>
                    <p className="card-text">{user.company.name}</p>
                    <p><a href="">{user.website}</a></p>
                    <a className="btn btn-primary">{user.email}</a>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="user-list">
                {this.props.users.map(this.renderUser)}
            </div>
        );
    }

}

// mapping users to state.users
function mapStateToProps(state) {
    return { users: state.users };
}

// exporting main props
export default connect (mapStateToProps, actions)(UserList);