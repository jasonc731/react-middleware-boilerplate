/**
 * Created by jasonchhagan on 7/24/16.
 */

// all the constants go in here
export const FETCH_USERS = 'FETCH_USERS';
export const FETCH_USERS_REJECTED = 'FETCH_USERS_REJECTED';
export const FETCH_USERS_FULFILLED = 'FETCH_USERS_FULFILLED';