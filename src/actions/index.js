/**
 * Created by jasonchhagan on 7/24/16.
 */

import { FETCH_USERS } from "./constants";
import axios from "axios";

export function fetchUsers() {
    // this is a promise
    const request = axios.get("https://jsonplaceholder.typicode.com/users");

    return {
        type: FETCH_USERS,
        payload: request
    };
}