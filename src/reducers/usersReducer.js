/**
 * Created by jasonchhagan on 7/24/16.
 */

import { FETCH_USERS, FETCH_USERS_FULFILLED, FETCH_USERS_REJECTED } from "../actions/constants";

// accepts the fetch users action type
export default function(state=[

], action) {
    switch(action.type) {
        // fetching users is pending
        case FETCH_USERS:
            return [...state, ...action.payload];
        case FETCH_USERS_REJECTED:
            return [...state, ...action.payload];
        case FETCH_USERS_FULFILLED:
            return [...state, ...action.payload.data]; // it's on the data
    }

    return state;
};