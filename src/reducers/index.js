/**
 * Created by jasonchhagan on 7/24/16.
 */

import { combineReducers } from "redux";
import usersReducer from "./usersReducer";

const rootReducer = combineReducers({
    users: usersReducer
});

export default rootReducer;