/**
 * Created by jasonchhagan on 7/24/16.
 */

import {applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";
import reducers from "../reducers";
//import Async from "../middlewares/async";

// instead of promise(), you could also use Async
// set up proper logging for each state
const middleware = applyMiddleware(promise(), thunk, logger());

// exporting the store with the reducers and middleware
export default createStore(reducers, middleware);